
## 🚀 Starting Online Store

- **[Postman](https://www.postman.com/alwayssoyoung/workspace/online-store-api/overview).**

### Начало работы 

Для запуска проекта.

```bash
./start
```

## API Reference

### Регистрация пользователя

```http
  POST /api/register
```

| Parameter | Type     | Description                | Example        |
|:----------| :------- | :------------------------- |----------------|
| `name` | `string` | **Required**. | Name           |
| `email` | `string` | **Required**. | test@email.com |
| `password` | `string` | **Required**. | test-password  |
| `password_confirmation` | `string` | **Required**. | test-password |

### Вход пользователя

```http
  POST /api/login
```

| Parameter | Type     | Description  | Example        |
|:----------| :------- |:--------------|----------------|
| `email` | `string` | **Required**. | test@email.com |
| `password` | `string` | **Required**. | test-password  |

### Метод для получения дерева категорий.

```http
  GET /api/categories
```

### Метод для получения товаров

```http
  GET api/products
```

### Метод для добавления товара

```http
  POST api/products
```

### Метод для получения товара по slug

```http
  GET api/product/{slug}
```

### Метод для добавления товара в корзину (doesn't work right now)

```http
  PUT api/cart/{id}
```

