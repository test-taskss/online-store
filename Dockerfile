#FROM php:8.0-fpm-alpine
#
#RUN apt-get update && apt-get install -y \
#      nano \
#      git \
#      curl
#
#RUN docker-php-ext-install pdo_pgsql mbstring
#
#RUN curl -sS https://getcomposer.org/installer | php -- \
#     --install-dir=/usr/local/bin --filename=composer
#
#COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
#
#WORKDIR /app
#COPY . .
#RUN composer install
