<?php

namespace Database\Factories\User;

use App\Models\User\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class UserAddressesFactory extends Factory {
    public function definition() {
        return [
            'user_id' => User::all()->random()->id,
            'address_line' => $this->faker->streetAddress(),
            'city' => $this->faker->city(),
            'postal_code' => $this->faker->postcode(),
            'country' => $this->faker->country(),
            'phone_number' => $this->faker->phoneNumber(),
        ];
    }
}
