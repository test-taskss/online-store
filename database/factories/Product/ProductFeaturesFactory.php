<?php

namespace Database\Factories\Product;

use App\Models\Product\Products;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFeaturesFactory extends Factory {
    public function definition(): array {
        return [
            'product_id' => Products::all()->random()->id,
            'made_in' => $this->faker->country(),
            'length' => $this->faker->randomFloat(2, 1, 100),
            'width' => $this->faker->randomFloat(2, 1, 100),
            'height' => $this->faker->randomFloat(2, 1, 100),
            'weight' => $this->faker->randomFloat(2, 1, 100),
        ];
    }
}
