<?php

namespace Database\Factories\Product;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ProductsFactory extends Factory {
    public function definition(): array {
        $name = $this->faker->name();

        return [
            'category_id' => Category::query()->inRandomOrder()->first(),
            'name' => $name,
            'cost' => $this->faker->numberBetween(10000, 900000),
            'slug' => Str::slug($name),
            'in_stock' => $this->faker->numberBetween(0, 200),
            'description' => $this->faker->text(),
        ];
    }
}
