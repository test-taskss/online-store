<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up() {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('name', 255)->nullable(false);
            $table->string('slug')->unique('slug');
            $table->string('description', 512)->nullable();

            $table->unsignedInteger('in_stock')->default(0)->nullable(false);;
            $table->unsignedBigInteger('cost')->default(0)->nullable(false);

            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('products');
    }
};
