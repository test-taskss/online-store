<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up() {
        Schema::create('user_addresses', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('address_line', 80);
            $table->string('city');
            $table->string('postal_code');
            $table->string('country');
            $table->string('phone_number');

            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('user_addresses');
    }
};
