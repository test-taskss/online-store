<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up() {
        Schema::create('product_features', function (Blueprint $table) {
            $table->id();
            $table->foreignId('product_id')
                ->constrained()
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->string('made_in')->nullable(false);

            $table->float('length')->default(0.00)->nullable(false);
            $table->float('width')->default(0.00)->nullable(false);
            $table->float('height')->default(0.00)->nullable(false);
            $table->float('weight')->default(0.00)->nullable(false);

            $table->timestamps();
        });
    }

    public function down() {
        Schema::dropIfExists('product_features');
    }
};
