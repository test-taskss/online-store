<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product\ProductFeatures;
use App\Models\Product\Products;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Category::factory()->count(10)->create();
        Products::factory()->count(50)->create();
        ProductFeatures::factory()->count(50)->create();
    }
}
