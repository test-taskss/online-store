<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Public routes
Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);

Route::get('/categories', [CategoryController::class, 'index']);

Route::apiResource('/products', ProductsController::class)->only(['index', 'show', 'store']);
Route::get('/product/{slug?}', [ProductsController::class, 'showSlug']);

Route::put('/cart/{id}', [CartController::class, 'store']);


// Protected routes
Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::post('/logout', [AuthController::class, 'logout']);
//
//    Route::resource('/product', ProductsController::class)->only([
//        'create', 'update', 'destroy'
//    ]);
//
//
//    Route::resource('/category', CategoryController::class)->only([
//        'create', 'store', 'update', 'destroy'
//    ]);
});
