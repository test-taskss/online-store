<?php

namespace App\Traits\Resources;

trait Filtratable
{
    protected $fieldsToFiltrate = [];
    protected $filtrateMethod;

    /**
     * Set properties to exclude specified fields
     * @param  array|string $fieldsToFiltrate
     */
    public function except(...$fieldsToFiltrate)
    {
        $this->setFiltrateProps(__FUNCTION__, $fieldsToFiltrate);

        return $this;
    }

    /**
     * Set properties to make visible specified fields
     * @param mixed ...$fieldsToFiltrate
     * @return Filtratable [type] [description]
     */
    public function only(...$fieldsToFiltrate)
    {
        $this->setFiltrateProps(__FUNCTION__, $fieldsToFiltrate);

        return $this;
    }

    /**
     * Filtrate fields
     * @return array [filtrated data]
     */
    public function filtrateFields(array $data)
    {
        if ($this->filtrateMethod) {
            $filter = $this->filtrateMethod;
            return collect($data)->{$filter}($this->fieldsToFiltrate)->toArray();
        }

        return $data;
    }

    /**
     * Set properties for filtrating
     */
    protected function setFiltrateProps(string $method, array $fields): void
    {
        $this->filtrateMethod = $method;
        $this->fieldsToFiltrate = is_array($fields[0]) ? $fields[0] : $fields;
    }

    /**
     * Send fields to filtrate to Resource while processing the collection
     *
     * @param  $request
     * @return array
     */
    protected function processCollection($request)
    {
        if (! $this->filtrateMethod) {
            return $this->collection;
        }

        return $this->collection->map(function ($resource) use ($request) {
            $method = $this->filtrateMethod;
            return $resource->$method($this->fieldsToFiltrate)->toArray($request);
        })->all();
    }
}
