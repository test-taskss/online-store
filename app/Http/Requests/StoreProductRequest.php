<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'cost' => ['required', 'numeric'],
            'slug' => ['required', 'string', 'unique:products'],
            'in_stock' => ['integer'],
            'description' => ['required', 'string', 'max:512'],
            'category_id' => ['required', 'integer'],
        ];
    }
}
