<?php

namespace App\Http\Controllers;

use App\Models\Product\Products;
use App\Traits\HttpResponses;

class CartController extends Controller
{
    use HttpResponses;

    public function index() {
    }

    public function store($product_id) {
        $product = Products::find($product_id);

        if (!$product) {
            return $this->error('', 404, 'Товара с данным id='.$product_id.' не найдено.');
        }

        return $this->success('тут должен быть id корзины, для просмотра содержимого', 200, 'Товар добавлен в корзину!');
    }

    public function show($id) {
    }
}
