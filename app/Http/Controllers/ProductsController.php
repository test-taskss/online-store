<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Resources\Product\ProductCollection;
use App\Models\Product\Products;
use App\Traits\HttpResponses;
use Illuminate\Support\Str;

class ProductsController extends Controller {
    use HttpResponses;

    public function index() {
        return new ProductCollection(
            Products::all()
        );
    }

    public function store(StoreProductRequest $request) {
        $data = $request->validated();

        $product = new Products();
        $product->name = $data['name'];
        $product->category_id = $data['category_id'];
        $product->cost = $data['cost'];
        $product->slug = Str::slug($data['slug']);
        $product->in_stock = $data['in_stock'];
        $product->description = $data['description'];

        $product->setCreatedAt(now());
        $product->save();

        return $this->success('', 200, 'Запись успешно сохранена!');
    }

    public function showSlug($slug) {
        return new ProductCollection(
            Products::whereSlug($slug)
                ->get()
        );
    }
}
