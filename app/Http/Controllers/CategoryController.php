<?php

namespace App\Http\Controllers;

use App\Http\Resources\Category\CategoryCollection;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CategoryController extends Controller {
    public function index(): CategoryCollection {
        return new CategoryCollection(
            Category::query()
                ->where('parent_id', null)
                ->with('children')
                ->get()
        );
    }

    public function store(Request $request) {
        $category = new Category();

        $category->title = $request->title;
        $category->slug = Str::slug($request->title, '-');

        $category->save();
    }
}
