<?php

namespace App\Http\Resources\Product;

use App\Traits\Resources\Filtratable;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollection extends ResourceCollection
{
    use Filtratable;

    public function toArray($request): array {
        return [
            $this->processCollection($this->collection),
        ];
    }
}
