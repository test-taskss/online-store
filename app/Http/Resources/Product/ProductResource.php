<?php

namespace App\Http\Resources\Product;

use App\Models\Product\ProductFeatures;
use App\Models\Product\Products;
use App\Traits\Resources\Filtratable;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    use Filtratable;

    public function toArray($request)
    {
        return $this->filtrateFields([
            'category' => Products::find($this->id)->category->title,
            'products' => $this->name,
            'cost' => $this->cost,
            'in_stock' => $this->in_stock,
            'features' => ProductFeatureResource::collection(
                ProductFeatures::where('product_id', $this->id)->get()
            )
        ]);
    }
}
