<?php

namespace App\Http\Resources\Product;

use App\Traits\Resources\Filtratable;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductFeatureResource extends JsonResource
{
    use Filtratable;

    public function toArray($request)
    {
        return $this->filtrateFields([
            "made_in" => $this->made_in,
            "length" => $this->length,
            "width" => $this->width,
            "height" => $this->height,
            "weight" => $this->weight,
        ]);
    }
}
