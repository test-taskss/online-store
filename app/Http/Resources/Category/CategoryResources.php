<?php

namespace App\Http\Resources\Category;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResources extends JsonResource {
    public function toArray($request): array {
        return [
            'title' => $this->title,
            'children' => CategoryResources::collection($this->children),
        ];
    }
}
