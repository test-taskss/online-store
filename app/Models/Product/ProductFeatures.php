<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductFeatures extends Model
{
    use HasFactory;

    public function products(){
        return $this->hasMany(Products::class);
    }

    protected $fillable = [
        'made_in',
        'length',
        'width',
        'height',
        'weight'
    ];
}
