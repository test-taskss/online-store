<?php

namespace App\Models\Product;

use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'category_id',
        'cost',
        'in_stock',
        'description',
    ];

    public function category() {
        return $this->belongsTo(Category::class,'category_id', 'id');
    }

    public function features(){
        return $this->belongsTo(ProductFeatures::class);
    }

    public function rules()
    {
        return [
            //
        ];
    }
}
