<?php

namespace App\Models;

use App\Models\Product\Products;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;

    public $fillable = ['title','parent_id'];

    public function children() {
        return $this->hasMany(self::class,'parent_id');
    }

    public function products() {
        return $this->hasMany(Products::class, 'category_id', 'id');
    }
}
